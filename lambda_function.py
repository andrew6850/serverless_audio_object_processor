import boto3
import botocore
import os
import logging
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
from decimal import Decimal
from pprint import pprint

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# AWS configuration:
s3 = boto3.resource('s3')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ['DYNAMO_TABLE'])


def object_handler(event, context):
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']

        try:
            s3.Bucket(bucket).download_file(key, '/tmp/' + key)

        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

        #audio_object = open(('/tmp/' + key), 'rb')
        audio_object = ('/tmp/' + key)
        
        # Get EasyID3 tags from audio_object, then create dictionary data type:
        audio = EasyID3(audio_object)
        audio_attributes = dict(audio.items())

        # Create object and get bitrate, audio file duration:
        built_audio_object = MP3(audio_object)
        decimal_length = Decimal(str(built_audio_object.info.length))
        
        #Get information from ID3 tags and store:
        id3_audio_object = EasyID3(audio_object)
        #pprint(id3_audio_object)
        title = id3_audio_object["title"][0]
        artist = id3_audio_object["artist"][0]
        album = id3_audio_object["album"][0]
        track_number = int(id3_audio_object["tracknumber"][0])
        genre = id3_audio_object["genre"][0]
        copyright = id3_audio_object["copyright"][0]

        # Add all metadata to DynamoDB:
        response = table.update_item(
            UpdateExpression="set title = :t, artist = :a, album = :al, \
                copyright = :c, bitrate = :b, decimal_length = :l, \
                track_number = :tr, genre = :g",
            Key={
                'audio_object_name': key,
            },
            ExpressionAttributeValues={
                ':t': title,
                ':a': artist,
                ':al': album,
                ':tr': track_number,
                ':g': genre,
                ':c': copyright,
                ':b': built_audio_object.info.bitrate,
                ':l': decimal_length,
            },
            ReturnValues="UPDATED_NEW"
        )
            
        #Log context statistics from function context to CW Logs:
        logging.info("Lambda function context information:")
        logging.info(("Function name", context.function_name))
        logging.info(("Function version", context.function_version))
        logging.info(("Function memory limit in MB:", context.memory_limit_in_mb))
        logging.info(("AWS Request ID:", context.aws_request_id))
        logging.info(("Function remaining time in ms:", context.get_remaining_time_in_millis()))
        logging.info("Local Audio object name: " + audio_object)
        logging.info("/tmp contents are: ")
        logging.info(os.listdir('/tmp'))
        
        return {
        "statusCode": 200,
        "body": response
    }